# Apic Bot

Bot public dédié à la sécurité civile pour les risques de pluie-inondation

## Pour commencer

- Tout d'abord vous devez créer votre application discord [Create] (https://discord.com/developers/docs/game-sdk/applications) et notez le token de connection

- Installez le package git + npm install

- node index.js
