const { GatewayIntentBits, Partials } = require("discord.js");

const config = {
    "bot" : {
        "owner": "352450630268485632",
        "admins": [],
        "support": [],
        "bans": [],
        
        "prefix": "apic",
        "invite": "https://discord.com/oauth2/authorize?client_id=998338879868383263&scope=bot%20applications.commands",
        "discord": "https://discord.gg/4BXMDh56xf"
    },

    "channels": {
        "log": {
            "status": "961970005707223162",
            "support": "961970005707223162",
            "errors": "961970005707223162",
            "cmd": "961970005707223162",
            "guildCreate": "961970005707223162",
            "guildDelete": "961970005707223162",
            "event": "961970005707223162"
        }
    },

    "embed": {
        "color": "#ffffff",
        "empty": "\u200B"
    },

    intents: [
    GatewayIntentBits.Guilds,
    // Intents.FLAGS.GUILD_MEMBERS,
    // Intents.FLAGS.GUILD_BANS,
    // Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS,
    // Intents.FLAGS.GUILD_INTEGRATIONS,
    // Intents.FLAGS.GUILD_WEBHOOKS,
    // Intents.FLAGS.GUILD_INVITES,
    // Intents.FLAGS.GUILD_VOICE_STATES,
    // Intents.FLAGS.GUILD_PRESENCES,
    GatewayIntentBits.GuildMessages,
    // Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
    // Intents.FLAGS.GUILD_MESSAGE_TYPING,
    // Intents.FLAGS.DIRECT_MESSAGES,
    // Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
    // Intents.FLAGS.DIRECT_MESSAGE_TYPING
    ],
    partials: [Partials.Channel, Partials.Message]
};

module.exports = config;
