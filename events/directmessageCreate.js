module.exports = async (client, message, config) => {
    const embed = {
        color: config.embed.color,
        author: {
            name: message.author.tag,
            icon_url: message.author.avatarURL()
        },
        description: message.content,
        timestamp: new Date()
    }

    await client.channels.fetch(config.channels.log.support)
        .then(channel => channel.send({ embeds: [embed] }));

    message.reply("**ℹ | Votre message a bien été transmis à l'équipe de support !**");
};