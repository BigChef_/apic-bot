const { Permissions, EmbedBuilder } = require('discord.js');
const { channels } = require("../config.js");
const User = require('../modules/users');

async function sendMsg(client, guild) {
  const owner = guild.fetchOwner();
  const embed = new EmbedBuilder({
    title: guild.name + " • " + guild.id,
    color: "#00FF00",
    author: {
      name: owner.tag + " • " + owner.id,
    },
    thumbnail: {
      url: guild.iconURL()
    },
    fields: [
      {
        name: "DEJA ENREGISTRE",
        value: alreadyexist ? ":white_check_mark:" : ":x:"
      },
      {
        name: 'TIMESTAMP',
        value: `<t:${parseInt(Date.now() / 1000)}:F> •  <t:${parseInt(Date.now() / 1000)}:R>)`
      }
    ]
  })
  client.channels.fetch(channels.log.guildCreate)
    .then(c => c.send({ embeds: [embed] }));
}

module.exports = (client, guild) => {
  console.log(`[GUILD JOIN] ${guild.id} added the bot. Owner: ${guild.ownerId}`);

  User.write(guild.id, {})
  .then(() => {
    console.log(`[GUILD JOIN] ${guild.id} added to db.`);
    sendMsg(client, guild)
  })
  .catch(console.error);

};
