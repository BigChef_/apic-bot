const { GuildManager, EmbedBuilder } = require("discord.js");
const { channels } = require("../config.js");
const {remove} = require('./../modules/users');
/**
 * Create a new Guild, add channels and insert into dataBase
 * @param { Client } client 
 * @param { GuildManager} guild 
 * @returns 
 */
async function sendMsg(client, guild) {
  const embed = new EmbedBuilder({
    title: guild.name + " • " + guild.id,
    color: "#FF0000",
    thumbnail: {
      url: guild.iconURL()
    },
    fields: [
      {
        name: 'TIMESTAMP',
        value: `<t:${parseInt(Date.now()/1000)}:F> • <t:${parseInt(Date.now()/1000)}:R>)`
      }
    ]
  })
  client.channels.fetch(channels.log.guildDelete)
    .then(c => c.send({ embeds: [embed] })).catch(console.error);
}

module.exports = (client, guild) => {
  if (!guild.available) return;
  console.log(`[GUILD LEAVE] ${guild.id} removed the bot.`);

  remove(guild.id)
  .then(() => {
    console.log(`[GUILD LEAVE] ${guild.id} removed from db.`);
  })
  .catch(console.error);

};
