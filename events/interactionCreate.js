const { EmbedBuilder, Interaction } = require('discord.js')
const logger = require("../modules/Logger.js");
const { permlevel, permLevels } = require("../modules/functions.js");
const config = require("../config.js");
const typical_error = require('../modules/typical_error.js');
module.exports = async (client, interaction) => {
  //Mettre l'interaction en attente
  await interaction.deferReply();
  async function runCmd(){
    logger.log("[BOUTTON] " + cmdName + " run" , "log");
    const guild = interaction.guild;
    try {
      cmd.run({
        client: client,
        interaction: interaction,
        config: config,
        permLevel: level,
        automessage: false
      })
        .then(async (messageData) => {
          interaction.editReply(messageData).catch(err => {  
            console.log("EditReply catch", err);
            typical_error(client, interaction, err);
          })
        })
        .catch(err => {
          console.log("Cmd run catch", err);
          typical_error(client, interaction, err)
        })
  
      logger.log(`${permLevels.find(l => l.level === level).name} ${interaction.user.id} ran slash command ${cmdName}`, "cmd");

      if (guild && !config.bot.admins.includes(interaction.user.id)) {
  
      const embed = new EmbedBuilder({
          color: interaction.member.displayHexColor,
          title: "🤖 | " + commande,
          author: {
            name: interaction.user.tag + " • " + interaction.user.id,
            icon_url: interaction.user.displayAvatarURL({ format: 'png', dynamic: 'true', size: 2048 })
          },
          fields: [
            {
              name: 'TIMESTAMP',
              value: `<t:${parseInt(Date.now() / 1000)}:F> • <t:${parseInt(Date.now() / 1000)}:R>`
            },
            {
              name: 'GUILD',
              value: guild.name + " • " + guild.id
            },
            {
              name: 'CHANNEL',
              value: interaction.channel.name + " • " + interaction.channel.id
            }
          ]
        })
  
        await client.channels.fetch(config.channels.log.cmd)
          .then(c => {
            c.send({ embeds: [embed] })
          })
          .catch(console.error)
      }
  
    } catch (e) {
      typical_error(client, interaction, e);
      console.error(e);
    }
  }
  const level = permlevel(interaction);
  // SI BOUTON UNIQUEMENT
  if (interaction.isButton()) {
    // client.emit("buttonClicked", interaction);
    
    const cmdName = interaction.customId.split(" ")[0];
    logger.log("[BOUTTON] " + cmdName + " emitted" , "log");
    const cmd = require('../components/' + cmdName); //Cherche la commande

    if (!cmd) interaction.editReply({ embeds: [interactionNotFound], components: supportButton, ephemeral: true });
    else cmd.run(client, interaction).then(async (msg) => {
      interaction.editReply(msg);
    }).catch(async (err) => {typical_error(client, interaction, err)});

  }
  else if (!interaction.type === 2) interaction.editReply({ embeds: [interactionNotFound], components: supportButton, ephemeral: true });;

  const cmd = client.container.slashcmds.get(interaction.commandName);
  if (!cmd) interaction.editReply({ embeds: [interactionNotFound], components: supportButton, ephemeral: true });;
  const cmdName = interaction.commandName;
  logger.log("[SLASH] " + cmdName + " emitted" , "log");
  //Slash Commande validé
  const commande = cmdName.charAt(0).toUpperCase() + cmdName.slice(1).toLowerCase()
  
  /* Niveaux de permissions */
  const levelRequired = client.container.levelCache[cmd.conf.permLevel]
  if (level < levelRequired) {
    console.info(`${permLevels.find(l => l.level === level).name} ${interaction.user.id} has not permissions for ${cmdName}`, "cmd")
    interaction.editReply({
      content: `**❌ | Vous devez avoir la permission ${permLevels.find(l => l.level === levelRequired)} pour éxécuter cette commande ! Vous avez la permission ${permLevels.find(l => l.level === level).name}**`,
      ephemeral: true
    })
  }
  else if (!cmd.conf.enabled) interaction.editReply({content: `**❌ | Cette commande est désactivée ou en cours de développement !**`, ephemeral: true })
  else if (cmd && !interaction.guild && cmd.conf.guildOnly && !!guild.guild) interaction.editReply("**❌ | Cette commande n'est pas disponible en message privé !**");

  runCmd();  
};