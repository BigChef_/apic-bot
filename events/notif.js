const {EmbedBuilder} = require('discord.js');
const { time } = require('@discordjs/builders');
const {log} = require('../modules/Logger');
const errorSend = require('../modules/error_send.js');
const typical_error = require('../modules/typical_error.js');
const config = require("../config.js");

const User = require('../modules/users');

// function loggerEvent(client, type, length) {
//     const embed = new EmbedBuilder({
//         // color: config.embed.color,
//         title: "🤖 | " + type,
//         description: `${length} guildes ciblés.`,
//         fields: [
//             {
//                 name: 'TIMESTAMP',
//                 value: time(new Date(), 'F') + " • " + time(new Date(), 'R')
//             }
//         ]
//     })
//     client.channels.fetch(config.channels.log.event)
//         .then(async channel => {
//             channel.send({ embeds: [embed] });
//         })
//         .catch(err => log("[CONFIG] Problème avec le salon de log des events " + err.message, "warn"))
// }

module.exports = async (client, notif) => {
    const {type, data} = notif;
    // log("[Notif] " + type + " emitted" , "log");
    // loggerEvent(client, type, null);
    const cmd = require('./notifs/' + type);
    cmd(client, data, async(msg) => {
        client.channels.fetch(msg.channelId)
        .then(channel => {
            channel.send(msg).catch(console.error);
        });
    })
    .catch(async (err) => {
        //Erreur dans l'éxécution de la commande
        console.error("Erreur dans l'éxécution de la notification " + type + "\n" + err.stack)
        // typical_error(client, null, err);
    })
}