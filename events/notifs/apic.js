const { EmbedBuilder } = require('discord.js');
const User = require('../../modules/users');

function messageNiveau(niveau){
    switch(niveau) {
        case 0 : 
            return {
                title: "Service dégradé",
                description: "La detection des pluies intenses est difficile, restez vigilent en cas de phénomène dangereux"
            }
        case 1 : 
            return {
                color: 15105570,
                url: "https://apic-vigicruesflash.fr/?mode=apic&area=fr",
                title: "__Pluie inondation :__ De fortes précipitations susceptibles d'affecter les activités humaines sont attendues",
                description: "Des inondations importantes sont possibles dans les zones habituellement inondables\n"
                + "Des cumuls importants de précipitation sur de courtes durées, peuvent, localement, provoquer des **crues inhabituelles** de ruisseaux et fossés.\n"
                + "Risque de **débordement des réseaux d'assainissement**.\n"

                + "Les conditions de circulation routière peuvent être rendues difficiles sur l'ensemble du réseau secondaire et quelques perturbations peuvent affecter les transports ferroviaires en dehors du réseau « grandes lignes ».\n"
                
                + "Des coupures d'électricité peuvent se produire.",
                fields: [
                    {
                        name: "Déplacements",
                        value: "Evitez les déplacements\n"
                        + "Ne vous engagez pas sur une route immergée, même partiellement"
                    },
                    {
                        name: "Dans un abri",
                        value: "Eloignez vous des cours d'eau et des points bas, rejoignez un point haut ou abritez-vous à l'étage\n"
                        + "ne descendez pas dans les sous-sols\n"
                        + "mettez vos biens hors d'eau et localisez votre kit d'urgence\n"
                    },
                    {
                        name: "Chez soi",
                        value: "Fermez les portes et les fenêtres afin de prévenir toute inondation"
                    }
                ],
                thumbnail: {url: "https://i.imgur.com/5Tm915M.png"}
            }
            case 2 : 
            return {
                color: 15158332,
                url: "https://apic-vigicruesflash.fr/?mode=apic&area=fr",
                title: "Pluie inondation : De fortes précipitations susceptibles d' affecter les activités humaines et la vie économique sont attendues",
                description: "Des inondations très importantes sont possibles, y compris dans des zones rarement inondables, sur l'ensemble des bassins hydrologiques des départements concernés\n"
                + "Des cumuls très importants de précipitations sur de courtes durées peuvent localement provoquer des crues torrentielles de ruisseaux et fossés.\n"
                
                + "Les conditions de circulation routière peuvent être rendues extrêmement difficiles sur l'ensemble du réseau.\n"
                + "Risque de débordement des réseaux d'assainissement.\n"
                
                + "Des coupures d'électricité plus ou moins longues peuvent se produire.",
                fields: [
                    {
                        name: "Déplacements",
                        value: "Je n'utilise pas ma voiture"
                        + "Je ne vais pas chercher mes enfants à l'école"
                        + "Je ne m’engage pas sur une route immergée, même partiellement"
                    },
                    {
                        name: "Dans un abri",
                        value: "Je reste chez moi et je me tiens informé auprès des autorités"
                        + "Je m'éloigne des cours d'eau, des points bas et des ponts et je rejoins le point le plus haut possible\n"
                        + "Je me réfugie en étage, en dernier recours sur le toit, je ne descends pas dans les sous-sols\n"
                        + "J'évacue uniquement sur ordre des autorités en emportant mon kit d'urgence\n"
                    },
                    {
                        name: "Chez soi",
                        value: "Fermez les portes et les fenêtres afin de prévenir toute inondation"
                    }
                ],
                thumbnail: {url: "https://i.imgur.com/WCHrFVs.png"}
            }
            default: {
                return {
                    title: "Autre"
                }
            }
    }
}

module.exports = async (client, data, callback) => {
    const {code, niveau} = data;
    User.get("apic", code)
    .then(async guilds => 
        guilds.guilds.forEach(guild => {
            const embed = new EmbedBuilder(messageNiveau(niveau));
            callback({channelId: guild, embeds: [embed]})
        })
    )
    .catch(() => {
        console.log("Code insee " + code + " sans guilde");
    })
}