const {EmbedBuilder} = require('discord.js');
const User = require('../../modules/users');

function couleurToHexa(color) {
    if(color === "Vert") return 0x00FF00;
    else if(color === "Orange") return 0xFF6600;
    else if(color === "Jaune") return 0xFFFF00;
    else if(color === "Rouge") return 0xFF0000;
    else return 0xFFFFFF;
}
function colorTothumbnail(color) {
    if(color === "vert") return null;
    else if(color === "jaune") return "https://www.ardeche.gouv.fr/local/cache-vignettes/L300xH168/arton6306-5e06c.png?1659091893";
    else if (color === "orange") return "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSuhxQPhysnFIwctdQN7NTmlYaWkhNjzTeQ4g&usqp=CAU";
    else if(color === "rouge") return "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThi5KDEDAsGxCOErKQSFzzZUggE0-jU2A49g&usqp=CAU";
    else return "https://www.ardeche.gouv.fr/local/cache-vignettes/L300xH168/arton6306-5e06c.png?1659091893"
}

module.exports = async (client, data, callback) => {
    const {dep, couleur, nom_dept, risque_valeur, coul_vague, vigilanceconseil_texte, vigilancecommentaire_texte, dateprevue, dateinsert, daterun} = data;
    const ts = new Date(dateprevue);
    const embed = {
        color: couleurToHexa(couleur),
        url: "https://vigilance.meteofrance.fr/fr/gard/" + nom_dept,
        thumbnail: {
            url: colorTothumbnail(couleur)
        },
        author: {
            name: "Plus d'informations sur les données",
            url: "https://data.opendatasoft.com/explore/dataset/vigilance-meteorologique%40public/information/?disjunctive.coul_vague&disjunctive.risque_valeur",
            icon_url: "https://public.opendatasoft.com/favicon.ico"
        },
        title: `${nom_dept} (${dep}) : Vigilance ${couleur} ${risque_valeur}`,
        description: vigilancecommentaire_texte ? vigilancecommentaire_texte : "Aucune description",
        fields: [
            {
                name: "Conseils (Territoire national)",
                value: vigilanceconseil_texte
            },
            {
                name: "Vague submerssion",
                value: coul_vague,
                inline: true
            },
        ],
        timestamp: ts.toISOString(),
        footer: {text: "Valable jusqu'à "}
    }

    User.get("vigilance", dep)
    .then(async guilds => 
            guilds.guilds.forEach(guild => {
                callback({channelId: guild, embeds: [embed]})
            })
        )
        .catch(() => {
            console.log("Code departemental " + dep + " sans guilde");
        })
}