const config = require("../config.js");
const { EmbedBuilder } = require("discord.js");
const { time } = require('@discordjs/builders');
const logger = require("../modules/Logger.js");

const deploy = async (client) => {
  // const { container } = client;
  // const cmd = container.slashcmds;
  // const globalCmds = client.application.commands;

  //   await globalCmds.set(cmd.map(c => c.commandData))
  //     .catch(e => console.log(e));
  //   console.log("déploiement fait")
    return;
};


module.exports = async client => {
  const usersCount = client.guilds.cache.map(g => g.memberCount).reduce((a, b) => a + b)

  logger.log(`${client.user.tag}, ready to serve ${usersCount} users in  ${client.guilds.cache.size} servers.`, "ready");

  client.user.setPresence({
    status: "online",
    activities:
      [{
        name: `${client.guilds.cache.size} guilds`,
        type: 'COMPETING'
      }]
      // [{
      //     name: `🟠 Mise à jour / Maintenance en cours`,
      //     type: 'COMPETING'
      // }]
  })

  deploy(client)
  await client.channels.fetch(config.channels.log.status)
    .then(c => {
      const embed = new EmbedBuilder()
        .setColor(config.embed.color)
        .setTitle("✅ | BOT ON !")
        .setDescription(time(new Date(), 'R'))
        .setTimestamp();

      c.send({ embeds: [embed] })
    });

};
