require("dotenv").config();
const { Client, Collection } = require("discord.js");
const { readdirSync } = require("fs");
const { intents, partials } = require("./config.js");
const { permLevels } = require('./modules/functions.js');
const logger = require("./modules/Logger.js");
const client = new Client({ intents, partials });

const apic = require('./modules/apic');
const vigilance = require('./modules/vigilance');

const commands = new Collection();
const slashcmds = new Collection();

const levelCache = {};
for (let i = 0; i < permLevels.length; i++) {
    const thisLevel = permLevels[i];
    levelCache[thisLevel.name] = thisLevel.level;
}

client.container = {
    commands,
    slashcmds,
    levelCache
};

const init = async () => {
    return new Promise(async (resolve, reject) => {
        const slashFiles = readdirSync("./slash").filter(file => file.endsWith(".js"));
        for (const file of slashFiles) {
          const command = require(`./slash/${file}`);
          const commandName = file.split(".")[0];
          logger.log(`Loading Slash command: ${commandName}. 👌`, "log");
      
          client.container.slashcmds.set(command.commandData.name, command);
        }
      
        const eventFiles = readdirSync("./events/").filter(file => file.endsWith(".js"));
        for (const file of eventFiles) {
            const eventName = file.split(".")[0];
            logger.log(`Loading Event: ${eventName}. 👌`, "log");
            const event = require(`./events/${file}`);
            client.on(eventName, event.bind(null, client));        
        }


        client.login().then(async () =>{
            resolve(client);
            apic.cron(client);
        })
        .catch(async (err) => {
          logger.log(err, "error");
          process.exit(0);
        })
    })
};

async function start(c) {
    const di = await init();
    vigilance.cron(client);
}

start();
