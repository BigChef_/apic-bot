const axios = require('axios');
const logger = require("./Logger.js");
const https = require('https');
const httpsAgent = new https.Agent({ rejectUnauthorized: false });

async function getCommunes(client, key){
    return new Promise(async(resolve, reject) => {
        axios.get('https://apic-vigicruesflash.fr/static/carto/apic/fr/apic_fr_' + key + '.json', { httpsAgent })
        .then(async (res) => {
            if(res.data && res.data.grains){
                const grains = res.data.grains;
                for(const code in grains){
                    const niveau = Number(grains[code]);
                    if(niveau != -1){
                        logger.log("Event apic déclanché sur " + code, "log");
                        client.emit("notif", {type: "apic", data: {code, niveau: niveau}});
                    }
                }
                resolve();
            }
            else reject();
        })
        .catch(async(err) => console.error(err))
    })
}

async function run(client) {
    return new Promise(async(resolve, reject) => {
        axios.get('https://apic-vigicruesflash.fr/static/carto/apic/fr/apic_fr_reseaux.json', { httpsAgent })
        .then(async(res) => {
            if(res.data && res.data.reseaux && res.data.reseaux[0]){
                const near = res.data.reseaux[0];
                const n1 = Number(near["n_1"]);
                const n2 = Number(near["n_2"]);
                const ni = Number(near["n_i"]);
                
                const key = near["date"];
                module.exports.near = key;
                const date = near["date_str"];
                
                if((n1 > 0 || n2 > 0) && key != module.exports.near){
                    logger.log(`[APIC] refresh datant de ${date}, n1: ${n1}, n2: ${n2}, ni: ${ni}}`);
                    resolve(await getCommunes(client, key));            
                }
                else logger.log("[APIC] Pas de changement de données");
        }
        else reject()
        })
        .catch(async (res) => {
            console.error(res);
        })
    })
}
async function cron(client){
    logger.log("Apic cron started", "ready")
    setInterval(() => run(client), 60000);
}



module.exports = {run, getCommunes, cron, near: 0}