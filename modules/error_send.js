const config = require("../config.js");
const { EmbedBuilder } = require("discord.js");
const { supportButton } = require('./models.js');
const { remove } = require('./users');
const pattern = {
    problem: "⚠️ Problème rencontré",
    toResolve: "✅ Pour résoudre",
    toStop: "⛔ Ignorer l'erreur",
    toStopMsg: "⚠️ Si vous ignorez l'erreur, vos événements seront plus notifiés par Aaron le Mercenaire"
}

function embedByCode (client, err, guild, channel) {
    const code = err.code;
    switch(code){
        case 50001: 
            return {
                title: "Nous avons pas accès au salon",
                description: "Envoi échoué pour les notifications in-game",
                fields: [{
                    name: pattern.problem,
                    value: `
                        Pour envoyer nos messages automatiques,
                        Nous avons besoin d'avoir les permissions dans se salon. 
                        Actuellement nous avons aucun accès à celui-ci.`    
                },
                {
                    name: pattern.toResolve,
                    value: `Pour pouvoir avoir nos messages, **autorisez "Voir le salon" à Aaron le mercenaire** sur <#${channel.id}>`
                }],
                image: {
                    url: 'https://i.imgur.com/Txps7YA.png',
                }
            }
        case 50007:
            return;
        case 50008:
            return {
                title: "Nous ne pouvons pas envoyer de messages dans un salon vocal",
                description: "Envoi échoué pour les notifications in-game",
                fields: [{
                    name: pattern.problem,
                    value: "Compliqué d'envoyer un message sur un salon audio"
                },
                {
                    name: pattern.toResolve,
                    value: `vous pouvez changer de salon avec /setchannel all <#SalonId> sur le serveur ${guild.name}`
                }],
                image: {
                    url: 'https://i.imgur.com/NY3EtIH.png',
                }
            }
        case 50009:
            return {
                title: `Le niveau de vérification du Salon <#${channel.id} est trop élevé pour que vous puissiez y accéder.`,
                description: "Envoi échoué pour les notifications in-game",
                fields: [{
                    name: pattern.problem,
                    value: `Nous avons pas les permissions d'envoyer les messages sur ${guild.name} à cause des options de modération.`
                },
                {
                    name: pattern.toResolve,
                    value: "Essayez de modifier les permissions du bot"
                }]
            }
        case 50013:
            return {
                title: "Nous n'avons pas la permissions d'envoyer des messages",
                description: "Envoi échoué pour les notifications in-game",
                fields: [{
                    name: pattern.problem,
                    value: `
                        Nous avons besoin d'avoir les permissions dans se salon.
                        \nActuellement nous avons pas l'autorisation d'envoyer des messages dans celui-ci.
                    `
                },{
                    name: pattern.toResolve,
                    value: `Pour pouvoir avoir nos messages, autorisez "Envoyer des messages" à Aaron le mercenaire sur <#${channel.id}>`
                }],
                image: {
                    url: "https://i.imgur.com/3RL0ykK.png"
                }
            }
        case 50024:
            return {
                title: "Impossible d'envoyer des messages sur ce type de canal",
                description: "Envoi échoué pour les notifications in-game",
                fields: [{
                    name: pattern.problem,
                    value: `Discord ne nous autorise pas à poster sur le cannal <#${channel.id}>`
                },
                {
                    name: pattern.toResolve,
                    value: `vous pouvez changer de salon avec /setchannel all <#SalonId> sur le serveur ${guild.name}`
                }],
                image: {
                    url: 'https://i.imgur.com/NY3EtIH.png',
                }
            }
        case 10003 : 
            return {
                title: "Salon supprimé",
                description: "Envoi échoué pour les notifications in-game",
                fields: [{
                    name: pattern.problem,
                    value: `Nous pouvons pas envoyer des messages dans un salon supprimé`
                },
                {
                    name: pattern.toResolve,
                    value: `vous pouvez changer de salon avec /setchannel all <#SalonId> sur le serveur ${guild.name}`
                }],
                image: {
                    url: 'https://i.imgur.com/NY3EtIH.png',
                }
            }
        case 10004 : 
            remove(guild);
            break;
        default:
            client.channels.fetch(config.channels.errors)
            .then(ch => {
                ch.send({
                    embeds: [new EmbedBuilder({
                        title: `Erreur ${code}`,
                        description: "Envoi échoué pour les notifications in-game\n" + err.stack,
                        fields: [{
                            name: 'TIMESTAMP',
                            value: `<t:${parseInt(Date.now()/1000)}:F> •  <t:${parseInt(Date.now()/1000)}:R>)`
                          },
                          {
                            name: 'GUILD',
                            value: guild.name + " • " + guild.id
                          },
                          {
                            name: 'CHANNEL',
                            value: channel.name + " • " + channel.id
                          }
                        ]
                    })]
                })
            }).catch(e => console.error(e.stack)); 
    }
}

/**
 * Permet d'envoyer un message au propriétaire du serveur si aaron n'arrive pas envoyer un message
 * dans un salon
 * 
 * IMPORTANT: gère les cas venant de discord uniquement
 * @param {*} client 
 * @param {*} channel 
 * @param {*} err 
 */
module.exports = async (client, err, data) => {
    console.error(err);
    const guild = data.guild;
    const channel = data.channel;
    const icon = guild ? guild.iconURL() : null;
    const text = embedByCode(client, err, guild, channel);
    
    if(text){
        text.fields.push({name: pattern.toStop, value: pattern.toStopMsg})
        const embed = new EmbedBuilder({
            author: {
                name: guild ? guild.name + " • " + guild.id : "guilde inconnue",
                icon_url: icon
            },
            color: "#FF0000",
            title: text.title,
            description: text.description,
            fields: text.fields,
            footer: {
                text: channel ? channel.name + " • " + channel.id : "Salon inconnu",
                icon_url: icon,
            },
            image: text.image
        })
        const owner = await guild.fetchOwner();
        // const owner = await client.users.fetch("352450630268485632") // BigChef pour tester
        owner.send({embeds: [embed], components: supportButton}).catch(e => {
            console.error("ErrorToSendError: ", e.stack)
        });
    }
    else console.error(err)
}