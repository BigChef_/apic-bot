const logger = require("./Logger.js");
const { Permissions } = require("discord.js");
const config = require("../config.js");
// Let's start by getting some useful functions that we'll use throughout
// the bot, like logs and elevation features.

/*
  PERMISSION LEVEL FUNCTION

  This is a very basic permission system for commands which uses "levels"
  "spaces" are intentionally left black so you can add them if you want.
  NEVER GIVE ANYONE BUT OWNER THE LEVEL 10! By default this can run any
  command including the VERY DANGEROUS `eval` and `exec` commands!

  */
const permLevels = [
  {
    level: 0,
    name: "Ban",
    check: () => true
  },

  {
    level: 1,
    name: "User",
    check: (message) => {
      const botUser = message.user ? message.user : message.author;
      return !config.bot.bans.includes(botUser.id);
    }
  },

  {
    level: 2,
    name: "Moderator",
    check: (message) => {
      try {
        return (message.member.permissions.has(Permissions.FLAGS.MANAGE_GUILD));
      } catch (e) {
        console.error(e)
        return false;
      }
    }
  },
  {
    level: 3,
    name: "Administrator",
    check: (message) => {
      try {
        return (message.member.permissions.has(Permissions.FLAGS.ADMINISTATOR));
      } catch (e) {
        return false;
      }
    }
  },
  {
    level: 4,
    name: "Proprietor",
    check: (message) => {
      const serverOwner = message.user ? message.user : message.author;
      return message.guild.ownerId === serverOwner.id;
    }
  },
  {
    level: 8,
    name: "Support",
    check: (message) => {
      const botSupport = message.user ? message.user : message.author;
      return config.bot.support.includes(botSupport.id);
    }
  },
  {
    level: 9,
    name: "Admin",
    check: (message) => {
      const botAdmin = message.user ? message.user : message.author;
      return config.bot.admins.includes(botAdmin.id);
    }
  },
  {
    level: 10,
    name: "Owner",
    check: (message) => {
      const owner = message.user ? message.user : message.author;
      return owner.id === config.bot.owner;
    }
  }
]

function permlevel(message) {
  let permlvl = 0;

  const permOrder = permLevels.slice(0).sort((p, c) => p.level < c.level ? 1 : -1);
  
  while (permOrder.length) {
    const currentLevel = permOrder.shift();
    if (message.guild && currentLevel.guildOnly) continue;
    if (currentLevel.check(message)) {
      permlvl = currentLevel.level;
      break;
    }
  }
  return permlvl;
}

/*
  GUILD SETTINGS FUNCTION

  This function merges the default settings (from config.defaultSettings) with any
  guild override you might have for particular guild. If no overrides are present,
  the default settings are used.

*/

// getSettings merges the client defaults with the guild settings. guild settings in
// enmap should only have *unique* overrides that are different from defaults.
function getSettings(guild) {
  settings.ensure("default", config.defaultSettings);
  if (!guild) return settings.get("default");
  const guildConf = settings.get(guild.id) || {};
  // This "..." thing is the "Spread Operator". It's awesome!
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
  return ({ ...settings.get("default"), ...guildConf });
}

/*
  SINGLE-LINE AWAIT MESSAGE

  A simple way to grab a single reply, from the user that initiated
  the command. Useful to get "precisions" on certain things...

  USAGE

  const response = await awaitReply(msg, "Favourite Color?");
  msg.reply(`Oh, I really love ${response} too!`);

*/
async function awaitReply(msg, question, limit = 60000) {
  const filter = m => m.author.id === msg.author.id;
  await msg.channel.send(question);
  try {
    const collected = await msg.channel.awaitMessages({ filter, max: 1, time: limit, errors: ["time"] });
    return collected.first().content;
  } catch (e) {
    return false;
  }
}


/* MISCELLANEOUS NON-CRITICAL FUNCTIONS */

// toProperCase(String) returns a proper-cased string such as: 
// toProperCase("Mary had a little lamb") returns "Mary Had A Little Lamb"
function toProperCase(string) {
  return string.replace(/([^\W_]+[^\s-]*) */g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
}

// These 2 process methods will catch exceptions and give *more details* about the error and stack trace.
process.on("uncaughtException", (err) => {
  const errorMsg = err.stack.replace(new RegExp(`${__dirname}/`, "g"), "./");
  logger.error(`Uncaught Exception: ${errorMsg}`);
  console.error(err);
  // Always best practice to let the code crash on uncaught exceptions. 
  // Because you should be catching them anyway.
  process.exit(1);
});

process.on("unhandledRejection", err => {
  logger.error(`Unhandled rejection: ${err}`);
  console.error(err);
});

module.exports = { getSettings, permlevel, awaitReply, toProperCase, permLevels };