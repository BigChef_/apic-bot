module.exports = function (mention) {
	// The id is the first and only match found by the RegEx.
	const matches = mention.match(/^<@!?(\d+)>$/);
	if (!matches) return mention;
	return matches[1];
}