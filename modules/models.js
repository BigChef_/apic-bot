const config = require("../config.js");
const { EmbedBuilder } = require("discord.js");

module.exports = {
    inviteButton: [{
        type: 1,
        components: [{
            type: 2,
            label: "📥 Inviter Aaron",
            style: 5,
            url: config.bot.invite
        }]
    }],
    supportButton: [{
        type: 1,
        components: [{
            type: 2,
            label: "👋 Discord du support",
            style: 5,
            url: config.bot.discord
        }]
    }],
}
