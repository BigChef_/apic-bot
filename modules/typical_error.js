const { EmbedBuilder } = require("discord.js");

module.exports = async (client, interaction, err) => {
  console.log("ERR DET")
  const embed = new EmbedBuilder({
    color: "#FF0000",
    title: ":x: Le message n'a pas pu s'envoyer",
    description: err ? err.name + " : " + err.message : "Inconnu"
  })
  interaction.editReply({ embeds: [embed], ephemeral: true });
}