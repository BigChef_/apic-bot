const fs = require('fs');

module.exports = {
    get: async function(table, key){
        return new Promise(async(resolve, reject) => {
            fs.readFile(`./data/${table}/${key}.json`, 'utf8', function(err, data) {
                if(!err) {
                    resolve(JSON.parse(data));
                }
                else if(err.code === "ENOENT"){
                    reject();
                }
                else{
                    console.error(!err);
                }
            });
        })
    },
    write: async function(table, key, data) {
        return new Promise(async(resolve, reject) => {
            fs.writeFile(`./data/${table}/${key}.json`, JSON.stringify(data), function(err, data) {
                if(err) reject(err);
                else resolve();
            })
        })
    },
    update: async function(table, key, data) {
        return new Promise(async(resolve, reject) => {
            fs.readFile(`./data/${table}/${key}.json`, 'utf8', function(err, userjson) {
                if(!err) {
                    //Update
                    const user = JSON.parse(userjson);
                    for(const value in data){
                        user[value] = data[value];
                    }
                    module.exports.write(key, user)
                    .then(resolve).catch(reject);
                }
                else if(err.code === "ENOENT") {
                    module.exports.write(table, key, data)
                    .then(resolve).catch(reject);
                }
            })
        })
    },
    append: async function(table, key, data) {
        return new Promise(async(resolve, reject) => {
            fs.readFile(`./data/${table}/${key}.json`, 'utf8', function(err, userjson) {
                //Update
                const user = err ? new Object() : JSON.parse(userjson);
                console.log(user);
                if(user["guilds"]){
                    if(!user["guilds"].find(x => x == data)) user["guilds"].push(data);
                }
                else user["guilds"] = [data];
                module.exports.write(table, key, user)
                .then(resolve).catch(reject);
            })
        })
    },
    pop: async function(table, key, data) {
        return new Promise(async(resolve, reject) => {
            fs.readFile(`./data/${table}/${key}.json`, 'utf8', function(err, userjson) {
                if(!err) {
                    //Update
                    const user = JSON.parse(userjson);
                    const codes = user["guilds"];
                    const codeBis = new Array();
                    for(const code of codes){
                        if(code != data) codeBis.push(code);
                    }
                    user["guilds"] = codeBis;
                    module.exports.write(table, key, user)
                    .then(resolve).catch(reject);
                }
                else if(err.code === "ENOENT") {
                    module.exports.write(table, key, data)
                    .then(resolve).catch(reject);
                }
            })
        })
    },
    remove: async function(table, key) {
        return new Promise(async (resolve, reject) => {
            fs.unlink(`./data/${table}/${key}.json`, (err) => {
                if(err) reject(err);
                else resolve() 
            })
        })
    }
}