const axios = require('axios');
var CronJob = require('cron').CronJob;

async function cron(client){
    var job = new CronJob(
        '* 30 6/16 * * *',
        function() {
            run(client);
        },
        null,
        true,
        'America/Los_Angeles'
    );
    job.start();
}

async function run(client){
    axios.get("https://data.opendatasoft.com/api/records/1.0/download/?disjunctive.coul_vague=true&disjunctive.risque_valeur=true&rows=1000&format=json&fields=dep,risque_valeur,vigilanceconseil_texte,couleur,nom_dept,vigilancecommentaire_texte,risque_valeur,daterun,dateinsert,dateprevue,coul_vague&dataset=vigilance-meteorologique@public&timezone=Europe/Berlin&lang=fr")
    .then(async(res) => {
        const data = res.data
        if(data){
            for(const record of data){
                
                const fields = record.fields;
                const {couleur, coul_vague} = fields;
                if(couleur !== "Vert" || coul_vague !== "Vert") client.emit("notif", {type: "vigilance", data: fields})
            }
        }
    })    
}

module.exports = {run, cron};