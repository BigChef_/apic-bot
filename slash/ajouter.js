const { EmbedBuilder } = require("discord.js");
const User = require('../modules/users');
const cmd = async function(client, interaction, code){
    return new Promise(async(resolve, reject) => {
        const regex = new RegExp(/([0-9]{2}|2A|2B)+[0-9]{3}/);
        if(regex.test(code) && code.length === 5){
            await User.append("apic", code, interaction.channel.id)
            .then(() => {}).catch(() => {})
            
            await User.append("vigilance", code.charAt(0)+code.charAt(1), interaction.channel.id)
            .then(() => {}).catch(() => {})
            
            resolve({embeds: [{
                title: "Ajouté avec succès",
                description: "vous serez prévenu des aléas sur ces lieux",
                color: 3066993
            }]})
        }
        else resolve({
            embeds: [{
                title: "Code INSEE incorrect",
                description: "Le code postal ne respecte pas le shéma des codes postaux",
                color: 15158332
            }]
        })
    })
    
}

exports.run = async (cmdData) => {
    return new Promise(async(resolve, reject) => {
        
        const {client, interaction} = cmdData
        const code = interaction.options.getString('code');

        cmd(client, interaction, code)
        .then(resolve).catch(e => reject);
    })
}

exports.commandData = {
    name: "ajouter",
    description: "Permet d'ajouter une commune",
    options: [
        {
            type: 3,
            name: "code",
            description: "Code INSEE",
            required: true
        }
    ],
    defaultPermission: true,
};

exports.conf = {
    permLevel: "User",
    enabled: true,
    guildOnly: true
};
